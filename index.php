<?php

/**
 * @defgroup plugins_generic_setReviewType setReviewType plugin
 */

/**
 * @file index.php
 *
 * @ingroup plugins_generic_setReviewType
 * @brief Wrapper for set review type plugin.
 *
 */
require_once('SetReviewTypePlugin.inc.php');

return new SetReviewTypePlugin();
