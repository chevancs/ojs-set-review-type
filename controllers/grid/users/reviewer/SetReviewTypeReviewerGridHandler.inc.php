<?php

/**
 * @file controllers/grid/users/reviewer/SetReviewTypeReviewerGridHandler.inc.php
 *
 * @class SetReviewTypeReviewerGridHandler
 * @ingroup plugins_generic_setReviewTypePlugin
 *
 * @brief Customized reviewer grid handler not displaying the review type.
 */

import('controllers.grid.users.reviewer.ReviewerGridHandler');

class SetReviewTypeReviewerGridHandler extends ReviewerGridHandler {
	//
	// Overridden methods from PKPHandler
	//
	/**
	 * @copydoc GridHandler::initialize()
	 */
	function initialize($request, $args = null) {
		parent::initialize($request, $args);

		// discard the review type column
		$columns =& $this->getColumns();
		unset($columns['method']);
	}
};
