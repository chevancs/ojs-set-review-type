<?php

/**
 * @file tests/functional/SetReviewTypeFunctionalTest.php
 *
 *
 * @class SetReviewTypeFunctionalTest
 * @ingroup plugins_generic_setReviewType
 *
 * @brief Functional tests for the setReviewType plugin.
 */

import('tests.ContentBaseTestCase');

define('REVIEW_METHOD', 'Double-blind');

class SetReviewTypeFunctionalTest extends ContentBaseTestCase {
	/** @var string A hand-picked reviewer name from test database */
	protected $reviewer1 = 'Adela Gallego';

	/** @var string A hand-picked user name from test database to enroll as reviewer */
	protected $reviewer2 = 'Stephanie Berardo';

	/** @var string Title of a hand-picked submission in review stage from test database */
	protected $title = 'The influence of lactation on the quantity and quality of cashmere production';

	/** @var boolean Whether the plugin has been enabled for the context */
	private $pluginEnabled = false;

	/**
	 * @copydoc WebTestCase::getAffectedTables
	 */
	protected function getAffectedTables() {
		return PKP_TEST_ENTIRE_DB;
	}

	/**
	 * @see WebTestCase::setUp()
	 */
	protected function setUp() {
		parent::setUp();

		$this->start();

		$this->setDefaultReviewType(REVIEW_METHOD);
	}

	/**
	 * @see WebTestCase::tearDown()
	 */
	protected function tearDown() {
		$this->start();

		$this->enablePlugin(false);

		$this->stop();

		parent::tearDown();
	}

	/**
	 * Enable/disable the plugin
	 */
	private function enablePlugin($enable = true) {
		$this->logIn('admin', 'admin');

		$this->waitForElementPresent($selector = 'link=Website');
		$this->clickAndWait($selector);
		$this->click('link=Plugins');

		// find the plugin
		$this->waitForElementPresent($selector = '//input[starts-with(@id, \'select-cell-setreviewtypeplugin-enabled\')]');
		// enable/disable if necessary
		if ($enable && !$this->isChecked($selector)) {
			// do enable the plugin
			$this->click($selector);
			$this->waitForElementPresent('//div[contains(.,\'The plugin "Set Review Type" has been enabled.\')]');
		} else if (!$enable && $this->isChecked($selector)) {
			// do disable the plugin
			$this->click($selector);

			// confimation popup
			$this->waitForElementPresent('css=div.pkp_modal_confirmation');
			$this->click('css=a.pkpModalConfirmButton');
      $this->waitForElementNotPresent('css=div.pkp_modal_confirmation');

			$this->waitForElementPresent('//div[contains(.,\'The plugin "Set Review Type" has been disabled.\')]');
		}

		$this->pluginEnabled = $enable;

		$this->logOut();
	}

	/**
	 * Select a default review type in workflow settings.
	 */
	private function setDefaultReviewType($mode) {
		$this->logIn('admin', 'admin');

		// Settings > Workflow > Review
		$this->waitForElementPresent($selector = 'link=Workflow');
		$this->clickAndWait($selector);
		$this->click('link=Review');

		$this->waitForElementPresent('id=defaultReviewMode');
		$this->select('id=defaultReviewMode', "label=$mode");

		$this->click('//button[text()=\'Save\']');
		$this->waitForTextPresent('Your changes have been saved.');

		$this->logOut();
	}

	/**
	 * Helper to select a reviewer from list and test the review method.
	 * @see PKPContentBaseTestCase::assignReviewer()
	 */
	function selectReviewer($reviewer) {
		$this->waitForElementPresent($selector = 'css=[id^=component-grid-users-reviewer-reviewergrid-addReviewer-button-]');
		$this->click($selector);
		$this->waitForElementPresent('css=div.pkpListPanel--selectReviewer');
		$this->type('css=div.pkpListPanel--selectReviewer input.pkpListPanel__searchInput', $reviewer);
		$this->waitJQuery();
		$xpath = '//div[contains(text(),' . $this->quoteXPath($reviewer) . ')]';
		$this->waitForElementPresent($xpath);
		$this->click($xpath);
		$this->click('css=[id^=selectReviewerButton]');

		if ($this->pluginEnabled) {
			$this->assertElementNotPresent('//label[text()=\'Review Type\']');
		} else {
			$this->assertElementPresent('//label[text()=\'Review Type\']');
		}

		$this->click('//button[text()=\'Add Reviewer\']');
		$this->waitForElementNotPresent('css=div.pkp_modal_panel'); // pkp/pkp-lib#655
	}

	/**
	 * Helper to create a new reviewer and test the review method.
	 */
	function createNewReviewer($username, $email) {
		$this->waitForElementPresent($selector = 'css=[id^=component-grid-users-reviewer-reviewergrid-addReviewer-button-]');
		$this->click($selector);
		$this->waitForElementPresent($selector = 'link=Create New Reviewer');
		$this->click($selector);
		$this->waitForElementPresent('css=[id=createReviewerForm]');
		$this->type('css=[id^=firstName-]', ucfirst($username));
		$this->type('css=[id^=lastName-]', ucfirst($username));
		$this->type('css=[id^=username-]', $username);
		$this->type('css=[id^=email-]', $email);

		if ($this->pluginEnabled) {
			$this->assertElementNotPresent('//label[text()=\'Review Type\']');
		} else {
			$this->assertElementPresent('//label[text()=\'Review Type\']');
		}

		$this->click('//button[text()=\'Add Reviewer\']');
		$this->waitForElementNotPresent('css=div.pkp_modal_panel'); // pkp/pkp-lib#655
	}

	/**
	 * Helper to turn an existing user into a reviewer and test the review method.
	 */
	function enrollExistingUser($name) {
		$this->waitForElementPresent($selector = 'css=[id^=component-grid-users-reviewer-reviewergrid-addReviewer-button-]');
		$this->click($selector);
		$this->waitForElementPresent($selector = 'link=Enroll Existing User');
		$this->click($selector);
		// autocomplete: type name, wait, select user
		$this->waitForElementPresent('css=[id=enrollExistingReviewerForm]');
		$this->typeText('css=[id^=userId_input-]', substr($name, 0, 4));
		$this->waitForElementPresent($selector = 'css=.ui-autocomplete li:contains(\'' . $name . '\')');
		$this->click($selector);

		if ($this->pluginEnabled) {
			$this->assertElementNotPresent('//label[text()=\'Review Type\']');
		} else {
			$this->assertElementPresent('//label[text()=\'Review Type\']');
		}

		$this->click('//button[text()=\'Add Reviewer\']');
		$this->waitForElementNotPresent('css=div.pkp_modal_panel'); // pkp/pkp-lib#655
	}

	/**
	 * Edit a review assignment and test the review method.
	 */
	function editReviewer($name) {
		$this->waitForElementPresent($selector = 'css=tr:contains(\''. $name .'\') + tr a:contains(\'Edit\')');
		$this->click($selector);
		$this->waitForElementPresent('css=div.pkp_modal_panel div.header:contains(\'Edit Review\')');
		$this->waitJQuery();

		if ($this->pluginEnabled) {
			$this->assertElementNotPresent('//label[text()=\'Review Type\']');
		} else {
			$this->assertElementPresent('//label[text()=\'Review Type\']');
		}

		$this->click('//button[text()=\'OK\']');
		$this->waitForElementNotPresent('css=div.pkp_modal_panel'); // pkp/pkp-lib#655
	}

	/**
	 * Check the review type can be selected or changed without the plugin
	 */
	function testSetReviewType_noPlugin() {
		$this->findSubmissionAsEditor('dbarnes', null, $this->title);

		// 'Add Reviewer' popup: Select Reviewer
		$this->selectReviewer($this->reviewer1);

		// 'Reviewers' type column
		$this->assertElementPresent('css=tr:contains(\''. $this->reviewer1 .'\') span[id^=\'cell-\'][id$=\'-method\']:contains(\''.REVIEW_METHOD.'\')');

		// 'Edit Review' popup
		$this->editReviewer($this->reviewer1);

		// 'Add Reviewer' popup: Create New Reviewer
		$this->createNewReviewer('reviewer', 'reviewer@example.com');

		// 'Add Reviewer' popup: Enroll Existing User
		$this->enrollExistingUser($this->reviewer2);

		$this->logOut();
	}

	/**
	 * Check the review type can be neither selected nor changed with the plugin
	 */
	function testSetReviewType() {
		// enable the plugin
		$this->enablePlugin(true);

		$this->findSubmissionAsEditor('dbarnes', null, $this->title);

		// 'Add Reviewer' popup: Select Reviewer
		$this->selectReviewer($this->reviewer1);

		// 'Reviewers' type column
		$this->assertElementNotPresent('css=tr:contains(\''. $this->reviewer1 .'\') span[id^=\'cell-\'][id$=\'-method\']:contains(\''.REVIEW_METHOD.'\')');

		// 'Edit Review' popup
		$this->editReviewer($this->reviewer1);

		// 'Add Reviewer' popup: Create New Reviewer
		$this->createNewReviewer('reviewer', 'reviewer@example.com');

		// 'Add Reviewer' popup: Enroll Existing User
		$this->enrollExistingUser($this->reviewer2);

		$this->logOut();
	}
}
