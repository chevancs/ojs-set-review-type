<?php

/**
 * @file SetReviewTypePlugin.inc.php
 *
 * @class SetReviewTypePlugin
 * @ingroup plugins_generic_setReviewTypePlugin
 *
 * @brief Plugin for applying a journal-wide review type to reviews
 */

import('lib.pkp.classes.plugins.GenericPlugin');

class SetReviewTypePlugin extends GenericPlugin {

	//
	// Implement methods from Plugin
	//
	/**
	 * @copydoc Plugin::register()
	 */
	function register($category, $path, $mainContextId = null) {
		if (parent::register($category, $path, $mainContextId)) {
			if ($this->getEnabled($mainContextId)) {
				HookRegistry::register('TemplateManager::fetch', array($this, 'removeReviewType'));
				HookRegistry::register('TemplateManager::include', array($this, 'removeReviewType'));

				HookRegistry::register('createreviewerform::readuservars', array($this, 'overrideReviewMethod'));
				HookRegistry::register('enrollexistingreviewerform::readuservars', array($this, 'overrideReviewMethod'));
				HookRegistry::register('editreviewform::readuservars', array($this, 'overrideReviewMethod'));
				HookRegistry::register('advancedsearchreviewerform::readuservars', array($this, 'overrideReviewMethod'));

				HookRegistry::register('LoadComponentHandler', array($this, 'concealReviewType'));
			}
			return true;
		}
		return false;
	}

	/**
	 * @copydoc Plugin::getDescription()
	 */
	public function getDescription() {
		return __('plugins.generic.setReviewType.description');
	}

	/**
	 * @copydoc Plugin::getDisplayName()
	 */
	public function getDisplayName() {
		return __('plugins.generic.setReviewType.displayName');
	}

	//
	// Override methods from LazyLoadPlugin
	//
	/**
	 * @copydoc LazyLoadPlugin::setEnabled()
	 */
	function setEnabled($enabled) {
		// force recompilation of templates
		$request = Application::getRequest();
		$templateMgr = TemplateManager::getManager($request);
		$templateMgr->clearTemplateCache();

		return parent::setEnabled($enabled);
	}


	/**
	 * Remove review type selector from templates.
	 * 
	 * @param $hookName string TemplateManager::fetch or TemplateManager::include
	 * @param $args array
	 * @return boolean
	 * @see PKPTemplateManager::fetch(), PKPTemplateManager::include()
	 */
	function removeReviewType($hookName, $args) {
		$templateManager = $args[0];
		switch ($hookName) {
			case 'TemplateManager::fetch':
				$template = $args[1];
				break;
			case 'TemplateManager::include':
				$template = $args[1]['smarty_include_tpl_file'];
				break;
			default:
				assert(true);
				return false;
		}

		// only apply the filter to relevant templates
		if (
			$template === 'controllers/grid/users/reviewer/form/editReviewForm.tpl' ||
			$template === 'controllers/grid/users/reviewer/form/reviewerFormFooter.tpl'
			) {
			$templateManager->register_prefilter(array($this, 'removeReviewTypePrefilter'));
		}

		return false;
	}

	/**
	 * Smarty prefilter removing the review type form section.
	 * 
	 * @param $source string the template filename
	 * @param $templateManager PKPTemplateManager
	 * @return string the expunged template
	 */
	function removeReviewTypePrefilter($source, $templateManager) {
		// same block for edit and reviewer select forms
		// Note: assume there is no nested fbvFormSection
		$source = preg_replace(';\{fbvFormSection[^\}]+title="editor.submissionReview.reviewType"[^\}]*\}.+\{/fbvFormSection\};sU', '', $source);

		// job done, dump filter
		$templateManager->unregister_prefilter(__FUNCTION__);

		return $source;
	}

	/**
	 * Bypass the reading of review method from user variables.
	 * 
	 * It prevents users from setting and changing the review type by
	 * abusing the form.
	 * 
	 * @param type $hookName editreviewform::readuservars or advancedsearchreviewerform::readuservars
	 * @param $args array [
	 *  @option $form Form the form
	 *  @option $vars the names of the variable to read
	 * ]
	 * @see Form::readUserVars()
	 */
	function overrideReviewMethod($hookName, $args) {
		$form =& $args[0];
		$vars =& $args[1];

		if (is_a($form, 'EditReviewForm')) {
			$reviewAssignment = $form->_reviewAssignment;
			$form->setData('reviewMethod', $reviewAssignment->getReviewMethod());
			unset($vars[array_search('reviewMethod', $vars)]);
		} else if (is_a($form, 'ReviewerForm') && array_search('reviewMethod', $vars)) {
			// get default review mode for journal
			$submission = $form->getSubmission();
			$contextDao = Application::getContextDAO() ;
			$context = $contextDao->getById($submission->getContextId());
			$reviewMethod = $context->getSetting('defaultReviewMode');
			if (!$reviewMethod) $reviewMethod = SUBMISSION_REVIEW_METHOD_BLIND;
			$form->setData('reviewMethod', $reviewMethod);
			unset($vars[array_search('reviewMethod', $vars)]);
		}
	}

	/**
	 * Inject custom handler to conceal the review type in reviewer grid handler.
	 * 
	 * @param $hookName string LoadComponentHandler
	 * @param $args array [
	 *  @option $component string the component handler
	 *  @option $op string the component operation
	 * ]
	 * @return boolean true to set a new component
	 * @see PKPComponentRouter::getRpcServiceEndpoint()
	 */
	function concealReviewType($hookName, $args) {
		$component =& $args[0];
		switch ($component) {
			// override the ReviewerGridHandler to not display review type
			case 'grid.users.reviewer.ReviewerGridHandler':
				$component = 'plugins.generic.setReviewType.controllers.grid.users.reviewer.SetReviewTypeReviewerGridHandler';
				return true;
		}
		return false;
	}
}
